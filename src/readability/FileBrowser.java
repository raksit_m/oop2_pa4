/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability;

import java.io.File;
import java.net.URL;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 * FileBrowser is used for searching files from directory.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.03.30
 */
public class FileBrowser {
	
	/**
	 * Getting file location from selected file.
	 * @return File location (path)
	 */
	public String getFileName() {
		JFileChooser fc = new JFileChooser();
		FileFilter filter = new TextFileFilter();
		fc.addChoosableFileFilter(filter);
		fc.setCurrentDirectory(new File("C:/"));
		int result = fc.showDialog(null, "Choose File...");
		if(result == JFileChooser.APPROVE_OPTION) 
			return fc.getSelectedFile().getAbsolutePath();
			else return null;
	}
	
	// TextFilter is used for creating choice that users can choose only text files.
	class TextFileFilter extends FileFilter {
		
		// Check that file is text files or not.
		public boolean accept(File f) {
			if(f.isDirectory()) return true;
			if(!f.isFile()) return false;
			String ext = getExtension(f);
			if(ext.equals("txt")) return true;
			if ( ext.equals("htm") ) return true;
			if ( ext.equals("html") ) return true;
			return false;
		}
		
		// Get type of files.
		public String getExtension(File f) {
			String filename = f.getName();
			int k = filename.lastIndexOf('.');
			if(k <= 0 || k >= filename.length()-1) return "";
			else return filename.substring(k+1).toLowerCase();
		}
		
		// Get type of URL.
		public String getExtension(URL url) {
			String urlname = url.toString();
			int k = urlname.lastIndexOf('.');
			if(k <= 0 || k >= urlname.length()-1) return "";
			else return urlname.substring(k+1).toLowerCase();
		}
		
		public String getDescription() {
			return "Text (.txt) Files";
		}
	}
}
