/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Counting the word by using State Machine.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.03.31
 */
public class WordCounter {
	State state;
	/** Temporary value for storing the amount of syllables of each word. */
	private int syllables;
	/** Total amount of syllables in the text. */
	private int totalSyllables;
	/** Total amount of words in the text. */
	private int totalWords;
	/** Total amount of sentences in the text. */
	private int totalSentences;
	/** Check that sentence has at least one word. */
	private boolean hasWord;
	
	private ReadabilityCalculator calculator;

	/**
	 * Initializing state as StartState.
	 */
	public WordCounter() {
		this.state = StartState;
		calculator = new ReadabilityCalculator(this);
	}

	/**
	 * Setting state depending on the condition.
	 * @param newState as a new state
	 */
	public void setState(State newState) {
		if(newState != state) newState.enterState();
		this.state = newState;
	}

	/**
	 * StartState is used for starting the WordCounter only.
	 */
	State StartState = new State() {

		public void handleChar(char c) {
			if(isVowel(c) || isY(c)) setState(VowelState);
			else if(isLetter(c)) setState(ConsonantState);
			else if(isDash(c)) setState(DashState);
			else setState(NonWordState);
		}

		public void enterState() {

		}

	};

	State ConsonantState = new State() {

		public void handleChar(char c) {
			if(isVowel(c) || isY(c)) setState(VowelState);
			else if(isLetter(c));
			else if(isDash(c)) setState(DashState);
			else setState(NonWordState);
		}

		public void enterState() {

		}

	};

	State MultipleVowelState = new State() {
		public void handleChar(char c) {
			if(isVowel(c));
			else if(isLetter(c)) setState(ConsonantState);
			else if(isDash(c)) setState(DashState);
			else setState(NonWordState);
		}

		public void enterState() {

		}
	};

	State DashState = new State() {
		public void handleChar(char c) {
			if(isVowel(c)) setState(VowelState);
			else if(isLetter(c)) setState(ConsonantState);
			else setState(NonWordState);
		}

		public void enterState() {

		}
	};

	State NonWordState = new State() {
		public void handleChar(char c) {

		}

		public void enterState() {
			syllables = 0;
		}
	};

	State VowelState = new State() {

		public void handleChar(char c) {
			if(isVowel(c)) setState(MultipleVowelState);
			else if(isLetter(c)) setState(ConsonantState);
			else if(isDash(c)) setState(DashState);
			else setState(NonWordState);
		}

		public void enterState() {
			syllables++;
		}	
	};

	/**
	 * Count syllables, words and sentences from texts.
	 * @param in as InputStream (can be file or URL)
	 */
	public void count(InputStream in) {
		BufferedReader breader = new BufferedReader(new InputStreamReader(in));
		while( true ) {
			String line;
			try {
				line = breader.readLine( );
				if (line == null) break;
				line = line.trim().replaceAll("[,()\"/]", " ");
				line = removeTrickyPunctuation(line);
				String[] array = line.split(" ");
				for(int i = 0; i < array.length; i++) {
					int count = countSyllables(array[i]);
					totalSyllables += count;
					totalWords += countWords(count);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Removing tricky punctuation (such as semi-colons, long dashes) from the text.
	 * @param line from BufferedReader
	 * @return line without tricky punctuation
	 */
	public String removeTrickyPunctuation(String line) {
		line = line.replaceAll("\'", "");
		line = line.replaceAll(";", "");
		line = line.replaceAll("---", " ");
		line = line.replaceAll("--", " ");
		return line;
	}

	/**
	 * Counting syllables of the word and sentences if it detects some punctuation.
	 * @param word which we would like to know how many syllables.
	 * @return syllables of that word
	 */
	public int countSyllables(String word) {
		state = StartState;
		syllables = 0;
		String regex = "[?.:!]";
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			if(regex.indexOf(c) >= 0 && state != NonWordState && hasWord) {
				word = word.replace(Character.toString(c), "");
				totalSentences++;
				hasWord = false;
				break;
			}
			state.handleChar(c);		
		}

		if(state == DashState) {
			return 0;
		}

		if(state == VowelState && syllables > 1) {
			if(word.endsWith("e")) {
				syllables--;
			}
		}
		return syllables;
	}

	/**
	 * Counting word related by syllables - if it's not a word then add to totalWords.
	 * @param syllables of word
	 * @return 1 if it's a word, otherwise return false
	 */
	public int countWords(int syllables) {
		if(syllables > 0) {
			hasWord = true;
			return 1;
		}
		return 0;
	}

	public int getCountSyllables() {
		return totalSyllables;
	}

	public int getCountWords() {
		return totalWords;
	}

	public int getCountSentences() {
		return totalSentences;
	}
	
	/**
	 * Getting result to be shown in either console or GUI.
	 * @param file
	 * @return result
	 */
	public String getResult(File file) {
		return String.format("Filename: %s\n"
				+ "Number of Syllables: %d\n"
				+ "Number of Words: %d\n"
				+ "Number of Sentences: %d\n"
				+ "Flesch Index: %.1f\n"
				+ "Readability: %s\n", file.getAbsolutePath(), getCountSyllables(), getCountWords(),
				getCountSentences(), calculator.getFleschIndex(), 
				ReadabilityDescription.getReadability(calculator.getFleschIndex()));
	}
	
	/**
	 * Getting result to be shown in either console or GUI.
	 * @param url
	 * @return result
	 */
	public String getResult(URL url) {
		return String.format("URL: %s\n"
				+ "Number of Syllables: %d\n"
				+ "Number of Words: %d\n"
				+ "Number of Sentences: %d\n"
				+ "Flesch Index: %.1f\n"
				+ "Readability: %s\n", url.toString(), getCountSyllables(), getCountWords(),
				getCountSentences(), calculator.getFleschIndex(), 
				ReadabilityDescription.getReadability(calculator.getFleschIndex()));
	}

		/**
		 * Checking that char is a vowel or not.
		 * @param c as char
		 * @return true if char is a vowel, otherwise return false
		 */
		private boolean isVowel(char c) {
			return ("AEIOUaeiou".indexOf(c) >= 0);
		}

		/**
		 * Checking that char is Y or not.
		 * @param c as char
		 * @return true if char is a Y, otherwise return false
		 */
		private boolean isY(char c) {
			return c == 'y' || c == 'Y';
		}

		/**
		 * Checking that char is a consonant or not.
		 * @param c as char
		 * @return true if char is a consonant, otherwise return false
		 */
		private boolean isLetter(char c) {
			return Character.isLetter(c);
		}

		/**
		 * Checking that char is a dash (-) or not.
		 * @param c as char
		 * @return true if char is a dash, otherwise return false
		 */
		private boolean isDash(char c) {
			return (c == '-');
		}
	}