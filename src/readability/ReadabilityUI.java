/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Graphical User Interface for Readability.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.03.31
 * 
 */
public class ReadabilityUI extends JFrame {
	
	JTextField tfFileName;
	JTextArea taResult;
	String name;

	public ReadabilityUI() {
		this.setTitle("Readability by Raksit Mantanacharu");
		initComponents();
	}

	public void initComponents() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel inputBar = new JPanel();
		inputBar.setLayout(new FlowLayout());
		JLabel label = new JLabel("File or URL Name");
		tfFileName = new JTextField(30);
		JButton btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new BrowseFileListener());
		JButton btnCount = new JButton("Count");
		btnCount.addActionListener(new CountListener());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ClearListener());

		inputBar.add(label);
		inputBar.add(tfFileName);
		inputBar.add(btnBrowse);
		inputBar.add(btnCount);
		inputBar.add(btnClear);

		panel.add(inputBar);

		taResult = new JTextArea(7, 60);
		taResult.setEditable(false);
		JScrollPane scroll = new JScrollPane(taResult);
		panel.add(scroll);

		this.add(panel);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	// Browse File from the directory.
	class BrowseFileListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			FileBrowser browser = new FileBrowser();
			tfFileName.setText((browser.getFileName()));
		}
	}
	
	// Count syllables, words and sentences from textField and show the result in textArea.
	class CountListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(tfFileName.getText().equals("")) {
				return;
			}
			WordCounter wc = new WordCounter();
			String input = tfFileName.getText();
			if(input.contains(":/")) {
				try {
					URL url = new URL(input);
					try {
						InputStream in = url.openStream();
						wc.count(in);
						taResult.setText(wc.getResult(url));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
			}
			else {
				File file = new File(input);
				try {
					InputStream in = new FileInputStream(file);
					wc.count(in);
					taResult.setText(wc.getResult(file));
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
			
		}
	}
	
	// Clear textField when user presses Clear button.
	class ClearListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			tfFileName.setText("");
			taResult.setText("");
		}
	}
}
