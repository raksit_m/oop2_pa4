/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability;

/**
 * Enumeration for storing Readability description related to flesch index.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public enum ReadabilityDescription {
	Graduate("College graduate", 30.0),
	College("College student", 50.0),
	HighSchool("High school student", 60.0),
	NinthGrade("9th grade student", 65.0),
	EighthGrade("8th grade student", 70.0),
	SeventhGrade("7th grade student", 80.0),
	SixthGrade("6th grade student", 90.0),
	FifthGrade("5th grade student", 100.0);


	public final String name;
	public final double maxScore;

	private ReadabilityDescription(String name, double maxScore) {
		this.name = name;
		this.maxScore = maxScore;
	}

	public String getName() {
		return name;
	}

	public double getMaxScore() {
		return maxScore;
	}

	/**
	 * Find the description for readability related to Fleasch Index.
	 * @param score (Flesch Index)
	 * @return description of Readability
	 */
	public static String getReadability(double score) {
		if(score < 0) return "Advanced degree graduate";
		for(ReadabilityDescription r : ReadabilityDescription.values()) {
			while(score <= r.getMaxScore()) return r.getName();	
		}
		return "4th grade student";
	}
}
