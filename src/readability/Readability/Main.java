/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability.Readability;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import readability.ReadabilityUI;
import readability.WordCounter;

/**
 * An application class for reading input files from command prompt or GUI.
 * @author Raksit Mantanacharu 5710546402
 * @version 2015.03.31
 */
public class Main {
	/**
	 * @param args as command line
	 */
	public static void main(String[] args) {
		WordCounter wc = new WordCounter();
		if(args.length > 0) {
			String input = args[0];
			if(input.contains(":/")) {
				try {
					URL url = new URL(input);
					try {
						InputStream in = url.openStream();
						wc.count(in);
						System.out.println(wc.getResult(url));
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
			else {
				File file = new File(input);
				try {
					InputStream in = new FileInputStream(file);
					wc.count(in);
					System.out.println(wc.getResult(file));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		else {
			ReadabilityUI ui = new ReadabilityUI();
		}
	}
}
