/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package readability;

import readability.WordCounter;

/**
 * Calculating Readability Flesch Index.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class ReadabilityCalculator {

	private WordCounter wc;

	public ReadabilityCalculator(WordCounter wc) {
		this.wc = wc;
	}

	public double getFleschIndex() {
		return 206.835 - 84.6*(wc.getCountSyllables()*1.0 / wc.getCountWords()) 
				- 1.015*(wc.getCountWords()*1.0 / wc.getCountSentences());
	}
}
